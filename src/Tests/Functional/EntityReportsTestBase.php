<?php

namespace Drupal\entity_reports\Tests\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class EntityReportsTestBase
 *
 * @package Drupal\entity_reports\Tests\Functional
 * @group entity_reports
 */
abstract class EntityReportsTestBase extends \Drupal\Tests\BrowserTestBase {

  use ContentTypeCreationTrait;

  /**
   * @param array $values
   * @param array $fields
   *
   * @return \Drupal\node\Entity\NodeType
   * @throws \Exception
   */
  public function createSampleContentType(array $values, array $fields) {
    if (!isset($values['type'])) {
      do {
        $id = strtolower($this->randomMachineName(8));
      } while (NodeType::load($id));
    }
    else {
      $id = $values['type'];
    }
    $values += [
      'type' => $id,
      'name' => $id,
    ];
    $type = NodeType::create($values);
    $status = $type->save();

    // node_add_body_field($type);
    foreach ($fields as $name => $definition) {
      $this->createField($type, $name, $definition);
    }

    if ($this instanceof TestCase) {
      $this->assertSame($status,
        SAVED_NEW,
        (new FormattableMarkup(
          'Created content type %type.',
          ['%type' => $type->id()])
        )->__toString()
      );
    }
    else {
      $this->assertEqual($status,
        SAVED_NEW,
        (new FormattableMarkup('Created content type %type.',
          ['%type' => $type->id()])
        )->__toString()
      );
    }

    return $type;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $type
   * @param $name
   * @param $definition
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\field\Entity\FieldConfig
   * @throws \Exception
   */
  public function createField(EntityInterface $type, $name, $definition) {
    // Add or remove the body field, as needed.
    $field_storage = FieldStorageConfig::loadByName('node', $name);
    $field = FieldConfig::loadByName('node', $type->id(), $name);
    if (empty($field)) {
      $field = FieldConfig::create($definition + [
          'field_storage' => $field_storage,
          'bundle' => $type->id(),
        ]);
      $field->save();
    }
    return $field;
  }
}
